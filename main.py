#!/usr/bin/env python

def get_functions_bin_results(dec_func, zero):
    bin_results = {}
    for i, f in enumerate(dec_func):
        bin_x = (zero + bin(i)[2:])[-len(zero):]
        bin_results[bin_x] = (zero + bin(f)[2:])[-len(zero):]

    return bin_results


def get_sorted_bin_inputs(base, inputs=None):
    inputs = inputs or []
    if base == '0' * len(base):
        inputs.append(base)

    for i, b in enumerate(base):
        if b == '0':
            combination = base[:i] + '1' + base[i + 1:]
            if combination not in inputs:
                inputs.append(combination)
            get_sorted_bin_inputs(combination, inputs)

    return sorted(inputs, key=lambda k: str(k.count('1')) + '_' + k)


def get_combinations(lst, combinations=None):
    combinations = combinations or []
    for i in range(len(lst)):
        combination = lst[:i] + lst[i + 1:]
        if combination and combination not in combinations:
            combinations.append(sorted(combination))
        get_combinations(combination, combinations)

    return sorted(combinations)


def get_anf_coefficients(bin_inputs, bin_results, func_number):
    coefficients = {}
    for bin_input in bin_inputs:
        a_name = 'a_'
        a_num = 0
        a_nums = []
        for x in range(len(bin_input) - 1, -1, -1):
            a_num += 1
            if bin_input[x] == '1':
                a_name += str(a_num)
                a_nums.append(a_num)

        all_a_nums = get_combinations(a_nums)

        if a_name == 'a_':
            a_name = 'a_0'
        else:
            all_a_nums += [[0]]

        all_a_names = list(map(lambda nums: 'a_' + ''.join(map(lambda num: str(num), nums)), all_a_nums))

        coefficients[a_name] = int(bin_results[bin_input][func_number])
        for other_a_name in all_a_names:
            coefficients[a_name] ^= int(coefficients[other_a_name])

    return coefficients


def get_anf(coefficients):
    anf = ''
    for a_name in coefficients.keys():
        c = coefficients[a_name]
        if c == 1:
            nums = a_name[2:]
            if nums == '0':
                anf += f'1 + '
            else:
                nums = nums[::]
                nums = ' '.join(map(lambda num: 'x' + num, nums))
                anf += f'{nums} + '
    return anf[:-3]


def and_xor(a, b):
    r = 0
    for b in bin(int(a, base=2) & int(b, base=2))[2:]:
        r ^= int(b)
    return r


def get_fourier_coefficients(bin_results, func_number):
    fourier = {}
    for a in bin_results.keys():
        c = 0
        for x, f in bin_results.items():
            c += int(f[func_number]) * (1 if and_xor(a, x) == 0 else -1)
        fourier[a] = c

    return fourier


def get_fourier(coefficients):
    res = []
    for a, c in coefficients.items():
        res.append(f'C{int(a, base=2)} = {c}')
    return ', '.join(res)


def main():
    zero = '0000'
    dec_func = [12, 15, 3, 13, 1, 5, 2, 11, 9, 7, 4, 6, 0, 10, 8, 14]

    bin_results = get_functions_bin_results(dec_func, zero)
    bin_inputs = get_sorted_bin_inputs(zero)

    print('\nZhegalkin polynomial:')
    for func_number in range(len(zero)):
        coefficients = get_anf_coefficients(bin_inputs, bin_results, func_number)
        anf = f'f{len(zero) - func_number} = ' + get_anf(coefficients)
        print(anf)

    print('\nFourier coefficients:')
    for func_number in range(len(zero)):
        coefficients = get_fourier_coefficients(bin_results, func_number)
        fourier = f'f{len(zero) - func_number}: ' + get_fourier(coefficients)
        print(fourier)


if __name__ == '__main__':
    main()
